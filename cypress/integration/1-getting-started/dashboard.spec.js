describe('Login page', () => {
  it('entering our dashboard', () => {
    cy.visit('/')
    cy.get('.userInput').type('Remus').should('have.value', 'Remus')
    cy.get('.passwordInput').type('Salut')
    cy.get('.loginBtn').click()
  })

  it('creates a new account', () => {
    cy.get('.addButton').click({multiple:true})
  })

  it('makes a new transaction to postman', () => {
    // cy.get('.addTransaction').click({multiple:true})
    cy.intercept({
      method: 'POST',
      url: '/transactions',
    })
  })
})
