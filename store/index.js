import Vue from 'vue'
import axios from 'axios'

export const state = () => ({
  balance: 28600,
  transactions: [],
  accounts: [],
  authenticated: false
})

export const mutations = {
  SET_TRANSACTIONS(state, payload) {
    state.transactions = payload
  },
  SET_AUTH(state, payload) {
    state.authenticated = payload
  },
  SET_SPAM(state, payload) {
    const foundIndex = state.transactions.findIndex(item => (item.transaction_id === payload))
    if (foundIndex > -1) {
      Vue.set(state.transactions[foundIndex], 'spam', true)
    }
  },
  DELETE_SPAM(state, payload) {
    const foundIndex = state.transactions.findIndex(item => (item.transaction_id === payload))
    if (foundIndex > -1) {
      Vue.set(state.transactions[foundIndex], 'spam', false)
    }
  },
  SET_ACCOUNTS(state, payload) {
    state.accounts = payload
  },
  CREATE_ACCOUNT(state, payload) {
    state.accounts.push(payload)
  },
  CREATE_TRANSACTION(state, payload) {
    state.transactions.push(payload)
  }
}

export const actions = {
  getAccounts({commit}) {
    return axios.get('http://localhost:4040/accounts')
      .then(response => {
        commit('SET_ACCOUNTS', response.data)
      })
  },
  createAccount({commit}) {
    return axios.post('http://localhost:4040/accounts', {
      "active": true,
      "holder": "Remus Ranca",
      "account_number": "4243830171874014",
      "balance": "0",
      "currency": "USD"
    }).then(response => {
      commit('CREATE_ACCOUNT', response)
    })
  },
  createTransaction({commit}) {
    return axios.post('https://4afc0cbe-8278-4b50-aac5-4af87ab26037.mock.pstmn.io/transactions', {
      "transaction_id": 1111,
      "transaction_type": "TESTING",
      "transaction_amount": "111111111111",
      "transaction_date": "20/04/2021",
      "transaction_status": "succeeded"
    }).then(response => {
      commit('CREATE_TRANSACTION', response)
    })
  },
  spamTransaction({commit}, item) {
    return axios.post('http://localhost:4040/transactions', item
    ).then(() => {
      // alert(item)
      commit('SET_SPAM', item)
    }).catch(error => {
      //set spam anyway
      console.log(error)
      commit('SET_SPAM', item)
    })
  },
  deleteSpamTransaction({commit}, item) {
    return axios.request('http://localhost:4040/transactions', {
      data: item,
      method: 'delete'
    })
    //return axios.delete('http://localhost:4040/transactions', item)
      .then(() => {
      commit('DELETE_SPAM', item)
    }).catch(error => {
      //set spam anyway
      console.log(error)
      commit('DELETE_SPAM', item)
    })
    // console.log('spammedTransactions', this.spammedTransactions)
  },
  getTransactions({commit}) {
    return axios.get('https://4afc0cbe-8278-4b50-aac5-4af87ab26037.mock.pstmn.io/transactions')
      .then(response => {
        commit('SET_TRANSACTIONS', response.data)
      }).catch(error => {
        console.log(error)
      })
  }
}

export const getters = {
  spammedTransactions: (state) => state.transactions.filter((item) => {
    return (item.spam === true)
  }),
  authenticated: state => state.authenticated
}
